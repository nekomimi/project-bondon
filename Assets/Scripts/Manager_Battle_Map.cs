﻿using UnityEngine;
using System.Collections;

public class Manager_Battle_Map : MonoBehaviour {

   public TileMap Tile_map;
   public Manager_UI Manager_UI;
   public Manager_Player Player_force;
   public Manager_Enemy Enemy_force;
   public PathTile Left_top_tile;
   public PathTile Right_bottom_tile;
   public PathTile[,] Tiles_matrix;
   public bool Start_battle;
   int width;
   int height;
   static public int Total_turn=0;
   static public int Total_turn_force;
   static public string State_battle = "Dis_active";
   public string Name_force_current;
   public Manager_Forces Current_forces;
	// Use this for initialization
	void Start () {

        //if (Start_battle == false)
          //  return;
        //Name_force_current = "Player";
        //Player_force.Start_Active();
        //Add_Matrix_Tiles(Left_top_tile.gameObject.transform.position, Right_bottom_tile.transform.position);
	
	}
    public void Add_Matrix_Tiles(Vector3 start, Vector3 end)
    {
         width = Mathf.Abs((int)(start.x - end.x));
        height = Mathf.Abs((int)(start.z - end.z));
        Tiles_matrix = new PathTile[height + 1, width + 1];
        foreach(Transform pt in Tile_map.instances)
        {
            int i = Mathf.Abs((int)(pt.position.z - Left_top_tile.transform.position.z));
            int j = Mathf.Abs((int)(pt.position.x - Left_top_tile.transform.position.x));
            Tiles_matrix[i, j] = pt.GetComponent<PathTile>();
        }
    }
   
	// Update is called once per frame
	void Update () {
       
      
	
	}
    public void Switch_Force(string tagForce)
    {

        if(tagForce=="Player_force")
        {
            Debug.Log("Player force has end turn");
           Player_force.Dis_Active();
           Enemy_force.Start_Active();
        }
        else
        {
            Debug.Log("Enemy force has end turn");
            Player_force.Start_Active();
            Enemy_force.Dis_Active();
        }

    }
    public void Find_Range_Move(Vector3 pos,int range)
    {
        range ++;
        int z =(int)(Mathf.Abs(pos.z - Left_top_tile.transform.position.z));
        int x=(int)(Mathf.Abs(pos.x - Left_top_tile.transform.position.x));
        int H_top = z - range;
        int H_bot = z + range;
        int V_left = x - range;
        int V_right = x + range;
        if(H_top<0)
        {
            H_top=0;
        }
        if(H_bot>height)
        {
            H_bot=height;
        }
        if(V_left<0)
        {
            V_left=0;
        }
        if(V_right<0)
        {
            V_right=width;
        }
        for(int i=H_top;i<H_bot;i++)
        {
            Tiles_matrix[i, 5].transform.GetChild(0).GetComponent<Renderer>().material.color = Color.red;
        }

      

    }
    public void Change_Turn_Action_For_Target(Character target)
    {
       // Debug.Log("change turn");
        target.On_Turn_Action();
    }
    public void End_Action()
    {
        Enemy_force.Char_selected = null;
        Player_force.Char_selected = null;
        int counter = 0;
        for (int i = 0; i < Current_forces.chars.Count; i++)
        {
            if(Current_forces.chars[i].State_char=="Dead" || Current_forces.chars[i].State_char=="Used"){
                counter++;
            }
        }
        if (counter == Current_forces.chars.Count)
        {
            Debug.Log("Changign force : " + Name_force_current);
            Change_Force(Name_force_current);
        }
    }
    public void End_Turn(Character Char,Vector3 start_pos,Vector3 end_pos)
    {
        Current_forces.Char_selected = null;
        Char.State_char = "Used";
        //.chars[index_char].State_char = "Used";
       // Debug.Log(Current_forces.chars[index_char].Name + " is Endturne");
       // Current_forces.chars[index_char].t
       // Debug.Log("index "+ index_char +"of current "+Current_forces.chars[index_char].Index );
       // Current_forces.chars[index_char].Tile_map.Update_Position(start_pos,end_pos);
        Char.Tile_map.Update_Position(start_pos, end_pos);
       // Current_forces.chars[index_char].gameObject.SetActive(false);
       // Debug.Log(" State : " + Current_forces.chars[index_char].State_char);  
       
        Total_turn_force--;
     /*   if (Total_turn_force == 0)
        {
            Debug.LogWarning("Changign force : "+Name_force_current);
            
            Change_Force(Name_force_current);
        }*/
    }
    public void Change_Force(string NameForce)
    {
        if(NameForce=="Player"){
            //Debug.Log("Enemy's Turn");
            Player_force.Dis_Active();
           // Enemy_force.Start_Active();
            Manager_UI.Anim.SetInteger("state", 1);   
        }
        else
        {
            Debug.Log("Player's Turn");
            Enemy_force.Dis_Active();
            //Player_force.Start_Active();
            Manager_UI.Anim.SetInteger("state", 2);
        }
    }

}
