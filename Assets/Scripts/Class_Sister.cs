﻿using UnityEngine;
using System.Collections;

public class Class_Sister : Class_Char {

	// Use this for initialization
	void Start () {
        Range_attack = 2;
        Attack_time = 1;
        Mp = 10;
        Range_move = 2;
        Def = 0;
        Attack = 1;
        Name_class = "Sister";
        Char_models.Add("staff");
        Char_models.Add("head");
        Char_models.Add("bottle");
        base.Start();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
