﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HP_Bar : MonoBehaviour {

	// Use this for initialization
    public Character Char;
    Texture2D Hp_frame;
    Texture2D Hp_texture;
        
    //float Width_hp_bar;
    float Cur_hp;
    float Max_hp;
    public RectTransform Canvas;
    RectTransform rectTransform;
    RectTransform HP_rectTransform;
    //public RectTransform Rectransform;
	void Start () {
        rectTransform =GetComponent<RectTransform>();
        HP_rectTransform = transform.FindChild("Hp_Bar_Base").GetComponent<RectTransform>();
        if(Char.tag=="Enemy")
            HP_rectTransform.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Enemy_Hpbar");
        else
            HP_rectTransform.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Player_Hpbar");
     
       
	}
	
	void Update () {
        if(Canvas==null)
            return;
        float delx =Canvas.sizeDelta.x/Screen.width;
        float dely = Canvas.sizeDelta.y/Screen.height;

        if (Char == null)
            return;
        Cur_hp = Char.Cur_hp;
        Max_hp = Char.Max_hp;
        Vector3 screenPos = Camera.main.WorldToScreenPoint(Char.transform.position);
        rectTransform.anchoredPosition = new Vector2(screenPos.x * delx, screenPos.y * dely);
        //Width_hp_bar = Cur_hp / Max_hp * Width_hp_bar;
        //Debug.Log(" width " + Width_hp_bar);
        HP_rectTransform.localScale =new Vector3(Cur_hp / Max_hp,1,1);
	}
    void OnGUI()
    {
       
    }
}
