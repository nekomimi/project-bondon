﻿using UnityEngine;
using System.Collections;

public class Class_Gladiator : Class_Char {

	// Use this for initialization
	public override void Start () {
        Attack = 6;
        Def = 3;
        Mp = 0;
        Range_attack = 1;
        Range_move = 3;
        Attack_time = 1;
        Name_class = "Gladiator";
        Char_models.Add("weapon");
        Char_models.Add("head");
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
