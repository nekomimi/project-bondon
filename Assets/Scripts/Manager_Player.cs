﻿using UnityEngine;
using System.Collections;

public class Manager_Player : Manager_Forces {

	// Use this for initialization
    public string State ="not_select_char";
	override public void Start () {
        Name_force = "Player";
        base.Start();
        
	}
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
	}
    public override void Handle()
    {
        base.Handle();
        InputHandle();
    }
    void InputHandle()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
           // Manager_battle.Switch_Force(tag);
            if (Char_selected != null && Char_selected.tag=="Player" && Char_selected.State_char=="Unused")
                return;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if (hit.collider.gameObject.GetComponent<Character>() == null)
                {
                    if (Char_selected != null)
                    {
                        Debug.Log("not char is selected");
                        Char_selected.Select_Char(false);
                        Char_selected = null;
                    }
                    return;
                }
                string tag = hit.collider.transform.tag;
                switch (tag)
                {
                    case "Player":
                        Char_selected = hit.collider.gameObject.GetComponent<Char_Player>();
                        hit.collider.gameObject.GetComponent<Char_Player>().Select_Char(true);
                        //Debug.Log("Select player");
                        break;
                    case "Enemy":
                        Debug.Log("Select Enemy");
                        Char_selected = hit.collider.gameObject.GetComponent<Char_Enemy>();
                        hit.collider.gameObject.GetComponent<Char_Enemy>().Select_Char(true);
                        break;
                    case "NPC":
                        Debug.Log("Selected NPC");
                        break;
                    default:
                        Debug.Log("not char is selected");
                        Char_selected.Select_Char(false);
                        Char_selected = null;
                        break;
                }
                  /*   if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
15.             
             Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);
             RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction);
             //Select Hool
             if (hit != null && hit.collider.transform.name == "Target") {
20.                 isHit = false;
                 //Destroy(GameObject.Find(hit.collider.gameObject.name));
                 if (anim.GetBool ("IsSelected") == false) {
                     anim.SetBool ("IsSelected", true);
                     isSelected=true;
25. 
                 } else {
                     anim.SetBool ("IsSelected", false);
                     isSelected=false;
                 }
30.             }

                */
        
            }
        }


    }
}
