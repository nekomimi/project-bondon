﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LoadingScreen : MonoBehaviour {

    public string levelToLoad;
    public GameObject background;
    public Text text;
    public GameObject ProgressBar;
    private int loadProgress = 0;


	// Use this for initialization
	void Start () {
        background.SetActive(false);
        
        ProgressBar.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {
	   // if (Input.GetKeyDown("space"))
       // {
            StartCoroutine(DisplayLoadingScreen(levelToLoad));
       // }
	}

    IEnumerator DisplayLoadingScreen(string level)
    {
        background.SetActive(true);
        ProgressBar.SetActive(true);

        ProgressBar.transform.localScale = new Vector3(loadProgress, ProgressBar.transform.localScale.y, ProgressBar.transform.localScale.z);
        text.text = "Loading: " + loadProgress + "%";

        AsyncOperation async = Application.LoadLevelAsync(level);
        while (!async.isDone)
        {
            loadProgress = (int)(async.progress * 100);
            text.text = "Loading: " + loadProgress + "%";
            ProgressBar.transform.localScale = new Vector3(async.progress, ProgressBar.transform.localScale.y, ProgressBar.transform.localScale.z);
            yield return null;
        }
    
    }

}
