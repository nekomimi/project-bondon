﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
	public float walkSpeed;
    public Character character;
	TileMap tileMap;
	List<PathTile> path = new List<PathTile>();
	LineRenderer lineRenderer;
    public string state;

	void Start()
	{
        walkSpeed = 3;
		lineRenderer = GetComponent<LineRenderer>();
		tileMap = FindObjectOfType(typeof(TileMap)) as TileMap;
		enabled = tileMap != null;
        character = GetComponent<Character>();


	}

	void Update()
	{
      
        
        if (character.State_action != "Move" || character.State_move =="End move")
        {
            return;
        }
      
		if (Input.GetMouseButtonDown(0) && character.Anim.GetInteger("state")!=1)
		{
			//var plane = new Plane(Vector3.up, Vector3.zero);
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			//float hit;
            bool miss = true;
			//if (plane.Raycast(ray, out hit))
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, LayerMask.GetMask("TileMap")))
			{
				//var target = ray.GetPoint(hit);
                int index = tileMap.GetIndex((int)hitInfo.transform.position.x,(int)hitInfo.transform.position.z);
                if (tileMap.instances[index].GetComponent<PathTile>().id == -1)
                {
                   // Debug.Log("Fail");
                    return;
                }
                //tileMap.instances[index].transform.GetChild(0).GetComponent<Renderer>().material.color = Color.red;
                //if (tileMap.instances[tileMap.GetIndex((int)target.x,(int) target.y)].GetComponent<PathTile>().id == -1)
                   // return;
                //PathTile tile = hitInfo.collider.GetComponentInParent<PathTile>();
                if (tileMap.FindPath(transform.position, hitInfo.point, path))
               // if (tileMap.FindPath(transform.position, target, path))
				{
                    
                   

					//lineRenderer.SetVertexCount(path.Count);
                    //Debug.Log("Testing "+ tileMap.instances[index].GetComponent<PathTile>().id);
                    if (path.Count > character.Range_move+1 || path.Count == 0 ||   tileMap.instances[index].GetComponent<PathTile>().id ==-1)
                    {
                        return;
                    }
                    miss = false;
                    (character as Char_Player).Show_Range_Move(Color.white);
                    character.Anim.SetInteger("state", 1);
                    //(character as Char_Player).Start_point = gameObject.transform.position;
                  

					StopAllCoroutines();
					StartCoroutine(WalkPath());
                   // 

				}
			}
            if (miss == true)
            {
                Debug.Log(" Misss");
                (character as Char_Player).Back_To_Start_Point();
                //character.State_action = "";
               // character.Select_Char("show_infor","");
            }
		}
       
	}

	IEnumerator WalkPath()
	{
		var index = 0;
		while (index < path.Count)
		{
			yield return StartCoroutine(WalkTo(path[index].transform.position));
			index++;
		}
        character.Anim.SetInteger("state", 0);
       character.Select_Char(true);
        character.State_move = "End move";
        /*character.Menu_function[character.Menu_function.IndexOf(new MyDictionary("Move",false))].Is_active = true;
        character.Manager_ui.Show_Infor_Panel(character.name, character.level, character.Cur_hp, character.Max_hp, character.Cur_mp, character.Max_mp,
        character.Def_point, character.Attack_point);
        character.Manager_ui.Show_Menu_Action_Panel(character.Menu_function);*/
       // state = "End_moving";
       // if (character.Manager_ui.gameObject.activeInHierarchy == false)
       // {
         //   character.Manager_ui.gameObject.SetActive(true);
       // character.Manager_ui.Show_Menu_Action_Panel(character.Menu_function);
       // }
           
        //(character as Char_Player).Manager_ui.gameObject.SetActive(true);
	}

	IEnumerator WalkTo(Vector3 position)
	{
		while (Vector3.Distance(transform.position, position) > 0.01f)
		{
			transform.position = Vector3.MoveTowards(transform.position, position, walkSpeed * Time.deltaTime);
            transform.LookAt(position);
			yield return 0;
		}
		transform.position = position;
	}

	/*void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		for (int i = 0; i < path.Count; i++)
		{
			Gizmos.DrawSphere(path[i].transform.position, 0.05f);
			if (i > 0)
				Gizmos.DrawLine(path[i - 1].transform.position, path[i].transform.position);
		}
	}*/
}
