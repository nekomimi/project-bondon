﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class Manager_UI : MonoBehaviour {

	// Use this for initialization
    public Manager_Game Manager_game;
    public Manager_Player manager_player;
    public Manager_Battle_Map Manager_battle_map;
    public Char_Player Char_player;
    public List<Position_appear> List_pos;
    public Transform Pos_list_hero;
    public Button Hero_btn;
    public Handle_Pos_Btn  [] List_pos_btn;
    public List<Handle_Pos_Btn> List_pos_btn2;
    List<Cursor_Pos_Follow> List_cursor;
    Cursor_Pos_Follow Cursor_pos_clone;
    public Animator Anim;
    HP_Bar Prefab_HP_bar;
    Character[] Chars;
    Position_appear[] Position_appears;
    List<Cursor_Pos_Follow> Cursor_poss=new List<Cursor_Pos_Follow>();
	void Start () {
        for (int i = 0; i < List_pos.Count; i++)
        {
            List_pos_btn[i].gameObject.SetActive(true);
            List_pos_btn[i].Index_pos = i;
            List_pos_btn[i].Index_hero = -1;
            List_pos_btn[i].Manager_UI = this;
            //Image cursor_pos = Instantiate(Cursor_pos);
            //cursor_pos.transform.SetParent(transform);
             //Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, List_pos[i].transform.position);
             //cursor_pos.GetComponent<RectTransform>().anchoredPosition = screenPoint; //-gameObject.GetComponent<RectTransform>().sizeDelta / 2f;
             //cursor_pos.GetComponent<RectTransform>().localScale = new Vector2(1, 1);
            List_pos_btn2.Add(List_pos_btn[i]);
        }
        int j = 0;
        for (int i = 0; i <Manager_game.Heros.Count ; i++)
        {
            int x=0;
            //x = i;
            if(i%4==0 && i!=0){
                j++;
                transform.GetChild(3).transform.GetChild(0).GetComponent<RectTransform>().sizeDelta +=new Vector2(0, 120);
                //x = 0; 
            }
            x = i % 4;
            Button Btn_hero = Instantiate(Hero_btn);
            Btn_hero.GetComponent<RectTransform>().SetParent(Pos_list_hero.GetComponent<RectTransform>());
            Btn_hero.GetComponent<RectTransform>().localScale = Vector3.one;
            Btn_hero.GetComponent<RectTransform>().anchoredPosition = new Vector2(x * 120, -j * 120);
            Btn_hero.GetComponent<Handle_Hero_Btn>().Index = i;
            Btn_hero.GetComponent<Handle_Hero_Btn>().Manager_UI = this;
            Btn_hero.GetComponent<Image>().sprite = Resources.Load<Sprite>("Avatar_" + Manager_game.Heros[i].Name);
        }
        Prefab_HP_bar = Resources.Load<HP_Bar>("UI/prefab/HP_Bar_Up");
        Cursor_pos_clone = Resources.Load<Cursor_Pos_Follow>("UI/prefab/Cursor_pos");
        Position_appears  = GameObject.FindObjectsOfType<Position_appear>();
        foreach (Position_appear poss in Position_appears)
        {
            Cursor_Pos_Follow Temp = Instantiate(Cursor_pos_clone);
            Temp.Pos_Target = poss.transform;
            Temp.Canvas = gameObject.GetComponent<RectTransform>();
            Temp.transform.SetParent(transform);
            Temp.transform.GetChild(0).GetComponent<Text>().text = poss.index;
            Cursor_poss.Add(Temp);
         //   Debug.Log("TEsting " + Cursor_poss.Count);
        }
	
	}
	
	// Update is called once per frame
	void Update () {
    
        
	}
  
    public void Show_Infor_Panel(Character Char,bool show)//(string name,int level, int curhp,int maxhp,int curmp,int maxmp,int def,int at)
    {
        if (show == false)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            return;
        }
        transform.GetChild(0).gameObject.SetActive(true);

        transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = (Sprite)Resources.Load("Avatar_" + Char.Name,typeof(Sprite));
        transform.GetChild(0).GetChild(1).GetComponent<Text>().text = name;
        transform.GetChild(0).GetChild(2).GetComponent<Text>().text ="Lv : "+ Char.level;
        transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "HP: " + Char.Cur_hp + " / " + Char.Max_hp;
        transform.GetChild(0).GetChild(4).GetComponent<Text>().text = "MP: " + Char.Cur_mp + " / " + Char.Max_mp;
        transform.GetChild(0).GetChild(5).GetComponent<Text>().text = "Def: "+Char.Def_point ;
        transform.GetChild(0).GetChild(6).GetComponent<Text>().text = "At: " + Char.Attack_point;
    }
    public void Show_Menu_Action_Panel(List<MyDictionary> Menu_function,bool show)
    {
        if (show == false)
        {
            transform.GetChild(1).gameObject.SetActive(false);
            return;
        }
        transform.GetChild(1).gameObject.SetActive(true);
        Char_player = (manager_player.Char_selected as Char_Player) ;
        //transform.GetChild(1).gameObject.SetActive(true);
        foreach (MyDictionary dd in Menu_function)
        {
         
            if (dd.Name == "Move")
            {
                if (dd.Is_active == false)
                    transform.GetChild(1).GetChild(0).gameObject.SetActive(true);
                else
                    transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
            }

            if (dd.Name == "Attack")
            {
                if (dd.Is_active == false)
                    transform.GetChild(1).GetChild(1).gameObject.SetActive(true);
                else
                    transform.GetChild(1).GetChild(1).gameObject.SetActive(false);
            }
            if (dd.Name == "Skill" )
            {
                if(dd.Is_active == false)
                    transform.GetChild(1).GetChild(2).gameObject.SetActive(true);
                else
                    transform.GetChild(1).GetChild(2).gameObject.SetActive(false);
            }
            if (dd.Name == "End turn")
            {
                if(dd.Is_active == false)
                    transform.GetChild(1).GetChild(3).gameObject.SetActive(true);
                else
                    transform.GetChild(1).GetChild(3).gameObject.SetActive(false);
            }
            if (dd.Name == "Back")
            {
                if (dd.Is_active == false)
                {
                    transform.GetChild(1).GetChild(4).gameObject.SetActive(true);
                }
                else
                    transform.GetChild(1).GetChild(4).gameObject.SetActive(false);

            }
            
        }
     /*   if (open == false)
        {
            transform.GetChild(1).gameObject.SetActive(false);
            return;
        }
        Char_player =(manager_player.Char_selected as Char_Player);
        transform.GetChild(1).gameObject.SetActive(true);
        switch (action)
        {
            case "":
               transform.GetChild(1).GetChild(4).gameObject.SetActive(false);

                break;
            case "Move":
               transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
               transform.GetChild(1).GetChild(4).gameObject.SetActive(true);
                break;
            case "Attack":
                transform.GetChild(1).GetChild(1).gameObject.SetActive(true);
                
                break;
            case "Back":
                Debug.Log("Testing ");
                transform.GetChild(1).GetChild(4).gameObject.SetActive(false);
                transform.GetChild(1).GetChild(0).gameObject.SetActive(true);
                break;
            case "Skill":
                break;

        
        }*/
      
    }
    public void On_Attack_Btn_Clicked()
    {
        Char_player.Select_Action("Attack");
        //transform.GetChild(1).gameObject.SetActive(false);
        //transform.GetChild(0).gameObject.SetActive(false);
        //Char_player.Select_Char("");
        //Char_player.State_action = "Attack";
        //Char_player.Menu_function["Attack"] = true;
        //Char_player.Show_Range_Attack(Color.yellow);
    }
    public void On_Move_Btn_Clicked()
    {
        //transform.GetChild(1).gameObject.SetActive(false);
        //transform.GetChild(0).gameObject.SetActive(false);
        Char_player.Select_Action("Move");
        //Char_player.Select_Char("",false,"Move");
        //Char_player.State_action = "Move";
       // Char_player.Select_Char("");
        //Char_player.Menu_function["Move"] = true;
        //Char_player.Show_Range_Move(Color.blue);
    }
    public void On_Skill_Btn_Clicked()
    {
        Char_player.Select_Action("Skill");
    }
    public void On_End_Turn_Btn_Clicked()
    {
        Char_player.Select_Action("End turn");

        //Char_player.Manager_battle.End_Turn(Char_player.Index, "Used");
    }
    public void On_Back_Btn_Clicked()
    {
        //transform.GetChild(1).gameObject.SetActive(true);
        //transform.GetChild(0).gameObject.SetActive(true);
        //Char_player.State_action = "Back";
        //Char_player.Menu_function["Back"] = true;
        Char_player.Select_Action("Back");
        //Char_player.Back_To_Start_Point();
        
    }
    public void On_Pos_1Btn_Clicked()
    {

    }
    public void Selected_Hero(int index_hero)
    {
        var exist_hero = false;
        for (int i = 0; i < List_pos_btn2.Count; i++)
        {
            if (List_pos_btn2[i].Index_hero == index_hero)
            {
                exist_hero = true;
            }
        }
        if (!exist_hero)
        {
            for (int i = 0; i < List_pos_btn2.Count; i++)
            {
                if (List_pos_btn2[i].Index_hero == -1)
                {
                    List_pos_btn2[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Avatar_" + Manager_game.Heros[index_hero].Name);
                    List_pos_btn2[i].Index_hero = index_hero;
                    break;
                }
             
            }
        }
         
        
    }
    public void Drop_Hero_Selected_Btn_Clicked(int index_pos, int index_hero)
    {
        //Debug.Log("Testing index pos :" + index_pos);
        List_pos_btn2[index_pos].Index_hero = -1;
        List_pos_btn2[index_pos].GetComponent<Image>().sprite = null;
        
    }
    public void On_Ready_Btn_Clicked()
    {
        transform.GetChild(2).gameObject.SetActive(false);
        transform.GetChild(3).gameObject.SetActive(false);
        transform.GetChild(4).gameObject.SetActive(false);
        foreach (Cursor_Pos_Follow Cur in Cursor_poss)
        {
            Cur.gameObject.SetActive(false);
        }
        for (int i = 0; i < List_pos_btn2.Count; i++)
        {
            if (List_pos_btn2[i].Index_hero == -1)
                continue;
            int index_char=List_pos_btn2[i].Index_hero;
            Char_Player char_p = Instantiate(Manager_game.Heros[index_char]);
            char_p.transform.position = List_pos[i].transform.position;
            char_p.Index = manager_player.chars.Count;
            //char_p.Manager_battle = Manager_battle_map;
            //Instantiate(Manager_game.Heros[index_char],List_pos[i].transform.position,Quaternion.identity);
           // manager_player.chars.Add(Manager_game.Heros[index_char]);
            StartCoroutine("Wait_For_Start",char_p);
            
        }
        StartCoroutine("Wait_For_Instantiate");

       
      
        
    }
    IEnumerator Wait_For_Start(Char_Player player)
    {
        yield return new WaitForSeconds(2);
        manager_player.chars.Add(player);
      //  Debug.Log("Testing " + player.Tile_map.tileSize);
    }
    IEnumerator Wait_For_Instantiate()
    {
        yield return new WaitForSeconds(6);
        Manager_Battle_Map.State_battle = "Active";
        Manager_battle_map.Change_Force("Player");
        Chars = GameObject.FindObjectsOfType<Character>();
        foreach (Character ch in Chars)
        {
            HP_Bar Hp = Instantiate(Prefab_HP_bar);
          
            Hp.transform.SetParent(transform);
            Hp.Canvas = gameObject.GetComponent<RectTransform>();
            //  Hp.Rectransform = gameObject.GetComponent<RectTransform>() ;
            Hp.Char = ch;
            Hp.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            //   Debug.Log(" hi ");
            // Instantiate(Prefab_HP_bar);
        }
    }
    public void On_Complete_Move_Enemy_Panel()
    {
        Debug.Log("Complete enemy");
        Anim.SetInteger("state", 0);
        Manager_battle_map.Enemy_force.Start_Active();
       
    }
    public void On_Complete_Move_Player_Panel()
    {
        Anim.SetInteger("state", 0);
        Debug.Log("Complete player");
        Manager_battle_map.Player_force.Start_Active();
        
    }
  
}
