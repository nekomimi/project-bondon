﻿using UnityEngine;
using System.Collections;

public class Class_Soldier :Class_Char {

	// Use this for initialization
	override public void Start () {
        Name_class = "Soldier";
        Char_models.Add("weapon");
        Char_models.Add("head");
        Attack = 3;
        Def = 3;
        Range_move = 3;
        Range_attack = 1;
        Mp = 0;
        Attack_time = 1;
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
