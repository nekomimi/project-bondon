﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Mode_AI_Normal : MonoBehaviour {

    Manager_Enemy Manager_enemy;
    Manager_Player Manager_player;
    TileMap Tile_map;
    public Char_Player Target;
    static public bool Had_Target;
    List<PathTile> Path=new List<PathTile>();
	// Use this for initialization
	void Start () {
        Manager_enemy = GetComponent<Manager_Enemy>();
        Tile_map = GameObject.Find("Tilemap").GetComponent<TileMap>();
        Manager_player = GameObject.Find("Player_Master").GetComponent<Manager_Player>();
       // Search_Target();
	}
	
	// Update is called once per frame
	void Update () {
        if (Manager_Battle_Map.State_battle == "Dis_active")
            return;
       if( Had_Target==false)
            Search_Target();
	}
    public void Search_Target()
    {
        Target_Nearest();
       // Debug.Log(" Target nearset is: "+Target.Name);
       // Manager_enemy.Move_Selected_Char(Target);
    }
    void Target_Nearest()
    {
   
        for (int i = 0; i < Manager_enemy.chars.Count; i++)
        {
            if (Manager_enemy.chars[i].State_char == "Used" || Manager_enemy.chars[i].State_char == "Dead")
                continue;
            if (Had_Target == true)
                break;
            for (int j = 0; j < Manager_player.chars.Count; j++)
            {
                Tile_map.FindPath(Manager_enemy.chars[i].transform.position, Manager_player.chars[j].transform.position, Path);
                if (Path.Count < Manager_enemy.chars[i].Rang_at + 1)
                {
                    Target = Manager_player.chars[j] as Char_Player;
                    Had_Target = true;
                    Manager_enemy.Char_selected = Manager_enemy.chars[i];
                    Manager_enemy.Attack_Selected_Char(Target);
                    //Debug.Log("Testing enemy char "+Manager_enemy.Char_selected.Name);
                    break;
                }
                else
                {
                    if (Path.Count < Manager_enemy.chars[i].Range_move + 1)
                    {
                        Target = Manager_player.chars[j] as Char_Player;
                        Had_Target = true;
                        Manager_enemy.Char_selected = Manager_enemy.chars[i];
                        Debug.Log("Move ");
                        Manager_enemy.Move_Selected_Char(Target);
                        break;
                    }
                    else
                    {
                        List<PathTile> Path_move = new List<PathTile>();
                        Tile_map.Find_Range_Move(Manager_enemy.chars[i].transform.position,Manager_enemy.chars[i].Range_move, ref Path_move);
                       // Debug.Log(" Testing : " + Path_move.Count);
                        if(Path_move.Count>0)
                        {
                            int min =0;
                            int index = 0;
                            List<PathTile> Path_count = new List<PathTile>();
                            for (int z = 0; z < Path_move.Count; z++)
                            {
                                Tile_map.FindPath(Path_move[z].transform.position, Manager_player.chars[j].transform.position, Path_count);
                               //Debug.Log(" TEsitng z: " +Path_count.Count);
                               if (z == 0)
                               {
                                   min = Path_count.Count;
                                   index = z;
                               }
                               else
                               {
                                   if (min > Path_count.Count)
                                   {
                                       min = Path_count.Count;
                                       index = z;
                                      // Debug.Log("Testing z " + min + "of  " + Path_move[z].transform.position);
                                   }
                               }
                               
                            }
                            Manager_enemy.Char_selected = Manager_enemy.chars[i];
                            Debug.Log("Testing pos of target " + Manager_player.chars[j].transform.position);
                         //   Debug.Log("Move Pos " + Path_move.Count + " of " + Path_move[index].transform.position +" index "+index);
                            Had_Target = true;
                            Manager_enemy.Move_Selected_Char(Path_move[index].transform.position);
                            break;
                        }
                        else
                        {
                            if(j== Manager_player.chars.Count-1)
                            {
                                Manager_enemy.chars[i].End_Turn();
                                break;
                            }
                        }

                    }
                }
            }
        }
       /* for(int i=0;i<Manager_enemy.chars.Count;i++){
            if(Manager_enemy.chars[i].State_char=="Used" || Manager_enemy.chars[i].State_char=="Dead")
                continue;
            for(int j=0;j<Manager_player.chars.Count;j++){
                Tile_map.FindPath(Manager_enemy.chars[i].transform.position,Manager_player.chars[j].transform.position,Path);
                min=Path.Count;
                index1 = i;
                index2 = j;
                break;
            }
        }
       
        for(int i=0;i<Manager_enemy.chars.Count;i++){
            if (Manager_enemy.chars[i].State_char == "Used" || Manager_enemy.chars[i].State_char == "Dead")
                continue;
            for(int j=0;j<Manager_player.chars.Count;j++){
                Tile_map.FindPath(Manager_enemy.chars[i].transform.position,Manager_player.chars[j].transform.position,Path);
                if(min>Path.Count)
                {
                    min=Path.Count;
                    index1 = i;
                    index2 = j;
                }
            }
        }*/
       
      //  return target;
        
    }
}
