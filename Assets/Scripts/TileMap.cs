﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TileMap : MonoBehaviour
{
	static Queue<PathTile> queue = new Queue<PathTile>();
	static List<PathTile> closed = new List<PathTile>();
	static Dictionary<PathTile, PathTile> source = new Dictionary<PathTile, PathTile>();
  
	public const int maxColumns = 10000;

	public float tileSize = 1;
	public Transform tilePrefab;
	public TileSet tileSet;
	public bool connectDiagonals;
	public bool cutCorners;

	public List<int> hashes = new List<int>(100000);
	public List<Transform> prefabs = new List<Transform>(100000);
	public List<int> directions = new List<int>(100000);
	public List<Transform> instances = new List<Transform>(100000);
    // edit
    public Transform ob;
    Vector3 Anchor_pathtile;
	void Start()
	{
		UpdateConnections();
       
        int minx =(int) instances[0].transform.position.x;
        int maxz =(int) instances[0].transform.position.z;
        for(int i=0;i<instances.Count;i++)
        {
            if((int)instances[i].transform.position.x<minx)
            {
                minx = (int)instances[i].transform.position.x;
            }
        }
        for (int i = 0; i < instances.Count; i++)
        {
            if ((int)instances[i].transform.position.x > maxz)
            {
                maxz = (int)instances[i].transform.position.z;
            }
        }
        Anchor_pathtile.x = minx;
        Anchor_pathtile.z = maxz;

	}

	public int GetHash(int x, int z)
	{
		return (x + TileMap.maxColumns / 2) + (z + TileMap.maxColumns / 2) * TileMap.maxColumns;
	}
	
	public int GetIndex(int x, int z)
	{
		return hashes.IndexOf(GetHash(x, z));
	}
	
	public Vector3 GetPosition(int index)
	{
		index = hashes[index];
		return new Vector3(((index % maxColumns) - (maxColumns / 2)) * tileSize, 0, ((index / maxColumns) - (maxColumns / 2)) * tileSize);
	}
	public void GetPosition(int index, out int x, out int z)
	{
		index = hashes[index];
		x = (index % maxColumns) - (maxColumns / 2);
		z = (index / maxColumns) - (maxColumns / 2);
	}

	public void UpdateConnections()
	{
		//Build connections
		PathTile r, l, f, b;
		for (int i = 0; i < instances.Count; i++)
		{
			var tile = instances[i].GetComponent<PathTile>();
			if (tile != null )
			{

				int x, z;
				GetPosition(i, out x, out z);
				tile.connections.Clear();
				r = Connect(tile, x, z, x + 1, z);
				l = Connect(tile, x, z, x - 1, z);
				f = Connect(tile, x, z, x, z + 1);
				b = Connect(tile, x, z, x, z - 1);
				if (connectDiagonals)
				{
					if (cutCorners)
					{
						Connect(tile, x, z, x + 1, z + 1);
						Connect(tile, x, z, x - 1, z - 1);
						Connect(tile, x, z, x - 1, z + 1);
						Connect(tile, x, z, x + 1, z - 1);
					}
					else
					{
						if (r != null && f != null)
							Connect(tile, x, z, x + 1, z + 1);
						if (l != null && b != null)
							Connect(tile, x, z, x - 1, z - 1);
						if (l != null && f != null)
							Connect(tile, x, z, x - 1, z + 1);
						if (r != null && b != null)
							Connect(tile, x, z, x + 1, z - 1);
					}
				}
			}
		}
	}

	PathTile Connect(PathTile tile, int x, int z, int toX, int toZ)
	{
		var index = GetIndex(toX, toZ);
		if (index >= 0)
		{
			var other = instances[index].GetComponent<PathTile>();
			if (other != null)
			{
                //other.transform.GetChild(1).GetComponent<Renderer>().material.color = Color.white;
				tile.connections.Add(other);
                
				return other;
			}
		}
		return null;
	}

	PathTile GetPathTile(int x, int z)
	{
		var index = GetIndex(x, z);
		if (index >= 0)
			return instances[index].GetComponent<PathTile>();
		else
			return null;
	}
	public PathTile GetPathTile(Vector3 position)
	{
		var x = Mathf.RoundToInt(position.x / tileSize);
		var z = Mathf.RoundToInt(position.z / tileSize);
		return GetPathTile(x, z);
	}
    PathTile Get_Range_Move(PathTile tile, int toX, int toZ,Color color)
    {
        var index = GetIndex(toX, toZ);
        if (index >= 0)
        {
            var other = instances[index].GetComponent<PathTile>();
            if (other != null)
            {
                //Debug.Log("Testing");
                //other.transform.GetChild(1).gameObject.SetActive(true);
                //if(other.id!=-1)
                    other.transform.GetChild(0).GetComponent<Renderer>().material.color =color;
               //tile.connections.Add(other);

                return other;
            }
        }
        return null;
    }
    public void Find_Range_Move(Vector3 postion,int range,Color color)
    {
       
   
        for (int i = 0; i < instances.Count; i++)
        {
            var tile = instances[i].GetComponent<PathTile>();
            if (tile != null && i==GetIndex((int)postion.x,(int)postion.z))
            {
                int x, z;
                GetPosition(i, out x, out z);
                List<PathTile> path = new List<PathTile>();
                for (int j = 0; j < instances.Count; j++)
                {
                    int tox, toz;
                    GetPosition(j, out tox, out toz);
                    var tile2 = instances[j].GetComponent<PathTile>();
                   if(!FindPath(new Vector3(x,0,z),new Vector3(tox,0,toz), path))
                        continue;

                    if (tile2 != null && tile2.id!=-1 && path.Count>0 &&path.Count<=range+1 )
                    {
                            PathTile pt = Get_Range_Move(tile,tox,toz,color);
                    }
                }
                
                break;
            }
        }
    }
    public void Find_Range_Move(Vector3 postion, int range,ref List<PathTile> Path_move)
    {


        for (int i = 0; i < instances.Count; i++)
        {
            var tile = instances[i].GetComponent<PathTile>();
            if (tile != null && i == GetIndex((int)postion.x, (int)postion.z))
            {
                int x, z;
                GetPosition(i, out x, out z);
                List<PathTile> path = new List<PathTile>();
                for (int j = 0; j < instances.Count; j++)
                {
                    int tox, toz;
                    GetPosition(j, out tox, out toz);
                    var tile2 = instances[j].GetComponent<PathTile>();
                    if (!FindPath(new Vector3(x, 0, z), new Vector3(tox, 0, toz), path))
                        continue;

                    if (tile2 != null && tile2.id != -1 && path.Count > 0 && path.Count <= range + 1)
                    {
                        Path_move.Add(tile2);
                       // PathTile pt = Get_Range_Move(tile, tox, toz, color);
                    }
                }

                break;
            }
        }
    }
    public void Find_Range_Attack(Vector3 position, int range, Color color)
    {
        for (int i = 0; i < instances.Count; i++)
        {
            var tile = instances[i].GetComponent<PathTile>();
            if (tile != null && i == GetIndex((int)position.x, (int)position.z))
            {
                int x, z;
                GetPosition(i, out x, out z);
                //List<PathTile> path = new List<PathTile>();
                for (int j = 0; j < instances.Count; j++)
                {
                    var tile2 = instances[j].GetComponent<PathTile>();
                    if (tile2 != null)
                    {
                        int tox, toz;
                        GetPosition(j, out tox, out toz);
                        int distan = Math.Abs(x - tox) + Math.Abs(z - toz);
                        if (distan <= range && distan > 0)
                        {
                           // Debug.Log("Testing range ");
                            PathTile pt = Get_Range_Move(tile, tox, toz, color);

                        }
                    }
                }

            }
        }
    }
	public bool FindPath(PathTile start, PathTile end, List<PathTile> path, Predicate<PathTile> isWalkable)
	{
		if (!isWalkable(end))
			return false;
		closed.Clear();
		source.Clear();
		queue.Clear();
		closed.Add(start);
		source.Add(start, null);
		if (isWalkable(start))
			queue.Enqueue(start);
		while (queue.Count > 0)
		{
			var tile = queue.Dequeue();
           
			if (tile == end)
			{
				path.Clear();
				while (tile != null )
				{
					path.Add(tile);
					tile = source[tile];
				}
				path.Reverse();
				return true;
			}
			else
			{
				foreach (var connection in tile.connections)
				{
					if (!closed.Contains(connection) && isWalkable(connection))
					{   
                        
						closed.Add(connection);
                        source.Add(connection, tile);
                    
					        queue.Enqueue(connection);
					}
				}
			}
		}
		return false;
	}
	public bool FindPath(PathTile start, PathTile end, List<PathTile> path)
	{
		return FindPath(start, end, path, tile => true);
	}
	public bool FindPath(Vector3 start, Vector3 end, List<PathTile> path, Predicate<PathTile> isWalkable)
	{
		var startTile = GetPathTile(start);
		var endTile = GetPathTile(end);
		return startTile != null && endTile != null && FindPath(startTile, endTile, path, isWalkable);
	}
	public bool FindPath(Vector3 start, Vector3 end, List<PathTile> path)
	{
		return FindPath(start, end, path, tile => true);
	}
    public void Update_Position(Vector3  startpos,Vector3 endpos)
    {
        
        instances[GetIndex((int)startpos.x, (int)startpos.z)].GetComponent<PathTile>().id = 0;
        instances[GetIndex((int)endpos.x, (int)endpos.z)].GetComponent<PathTile>().id = -1;
    }
    public void Update_Position_Dead(Vector3 pos)
    {
        instances[GetIndex((int)pos.x, (int)pos.z)].GetComponent<PathTile>().id = 0;
    }
    public Vector3 Find_Path_In_Range(Vector3 from, Vector3 to)
    {
        Vector3 position = Vector3.zero; 
        for (int i = 0; i < instances.Count; i++)
        {
            var tile = instances[i].GetComponent<PathTile>();
            if (tile != null && i == GetIndex((int)to.x, (int)to.z))
            {
                int x;
                int z;
                GetPosition(i, out x, out z);
                List<PathTile> path = new List<PathTile>();
                for (int j = 0; j < instances.Count; j++)
                {
                    int tox, toz;
                    GetPosition(j, out tox, out toz);
                    var tile2 = instances[j].GetComponent<PathTile>();
                    //Debug.Log("tile  " + tile.transform.position + " tile 2" + tile2.transform.position+ "color :"+color +" range "+range);
                    if (!FindPath(new Vector3(x, 0, z), new Vector3(tox, 0, toz), path))
                        continue;
                   // Debug.Log("Testing  "+ path.Count + " "+ tile2.transform.position );
                    if (tile2 != null &&  tile2.id !=-1 && path.Count < 3 )
                    {
                        position = tile2.transform.position;
                       // Debug.Log(" Testing pos " + position);
                        break;
                    }
                }
               // break;
            }
        }
        return position;
    }
   /* public Vector3 Find_Path_In_Range(Vector3 from, Vector3 to)
    {
        Vector3 position = Vector3.zero;
        for (int i = 0; i < instances.Count; i++)
        {
            var tile = instances[i].GetComponent<PathTile>();
            if (tile != null && i == GetIndex((int)to.x, (int)to.z))
            {
                int x;
                int z;
                GetPosition(i, out x, out z);
                List<PathTile> path = new List<PathTile>();
                for (int j = 0; j < instances.Count; j++)
                {
                    int tox, toz;
                    GetPosition(j, out tox, out toz);
                    var tile2 = instances[j].GetComponent<PathTile>();
                    //Debug.Log("tile  " + tile.transform.position + " tile 2" + tile2.transform.position+ "color :"+color +" range "+range);
                    if (!FindPath(new Vector3(x, 0, z), new Vector3(tox, 0, toz), path))
                        continue;
                    // Debug.Log("Testing  "+ path.Count + " "+ tile2.transform.position );
                    if (tile2 != null && tile2.id != -1 && path.Count < 3)
                    {
                        position = tile2.transform.position;
                        // Debug.Log(" Testing pos " + position);
                        break;
                    }
                }
                // break;
            }
        }
        return position;
    }*/
    void Convert_To_Maxtrix()
    {
        
    }
    public void Find_Positions_Can_Move_In_Range(Vector3 position,Vector3 to,int range,ref List<PathTile> path)
    {

      //  Debug.Log("Testing range " + range);
        for (int i = 0; i < instances.Count; i++)
        {
            var tile = instances[i].GetComponent<PathTile>();
            if (tile != null && i == GetIndex((int)position.x, (int)position.z))
            {
                int x, z;
                GetPosition(i, out x, out z);
                //List<PathTile> path = new List<PathTile>();
                for (int j = 0; j < instances.Count; j++)
                {
                    var tile2 = instances[j].GetComponent<PathTile>();
                    if (tile2 != null && tile2.id!=-1)
                    {
                        int tox, toz;
                        GetPosition(j, out tox, out toz);
                        int distan = Math.Abs(x - tox) + Math.Abs(z - toz);
                        if (distan <= range && distan > 0)
                        {
                            path.Add(tile2);

                        }
                    }
                }
            }
        }
        Debug.Log("Testing " + path.Count);
    

    }
}
