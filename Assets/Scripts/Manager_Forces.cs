﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Manager_Forces : MonoBehaviour {

	// Use this for initialization
    public string Name_force;
    public Manager_Battle_Map Manager_battle;
    public List<Character> chars;
    public string State_forces;
    public Character Char_selected;
    public bool Ready_Start;
	virtual public void Start () {
        State_forces = "Dis_active";
        Manager_battle = GameObject.Find("Manager_Battle").GetComponent<Manager_Battle_Map>();
	}
	
	// Update is called once per frame
	public virtual void Update () {
        if (State_forces != "Active")
              return ;
        Handle();
	}
    
    virtual public void Start_Active()
    {
        Debug.Log(Name_force + " Start");
        Manager_battle.Name_force_current =Name_force;
        State_forces = "Active";
        Manager_battle.Current_forces = this;
        int i=0;
        foreach(Character ch in chars)
        {
            ch.Index = i;
            i++;
            if (ch.State_char == "Dead")
                continue;
            ch.State_char = "Unused";
           // Debug.Log(ch.Name+" Unused");
          //  Debug.Log(ch.Tile_map);
            Manager_Battle_Map.Total_turn_force += 1;
        }
        Ready_Start = true;
        Char_selected =null;
    }
    public void Dis_Active()
    {
        State_forces = "Dis_active";
    }
    virtual public void Handle()
    {

    }
    public void Check_End_Force()
    {
        int count=0;
        foreach(Character ch in chars)
        {
            if (ch.State_char == "Is_action")
                count++;
        }
        if(count==chars.Count)
        {
            Manager_battle.Switch_Force(tag);
        }
    }
    
  
}
