﻿using UnityEngine;
using System.Collections;

public class Handle_Pos_Btn : MonoBehaviour {
    public Manager_UI Manager_UI;
    public int Index_pos;
    public int Index_hero;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void Drop_Hero_Selected_Btn_Clicked()
    {
        if (Index_hero == -1)
            return;
        Manager_UI.Drop_Hero_Selected_Btn_Clicked(Index_pos, Index_hero);
    }
}
