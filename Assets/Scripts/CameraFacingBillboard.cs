﻿using UnityEngine;
using System.Collections;

public class CameraFacingBillboard : MonoBehaviour {

	// Use this for initialization
    public Camera m_Camera;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.back,
           m_Camera.transform.rotation * Vector3.up);
	}
}
