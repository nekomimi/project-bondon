﻿using UnityEngine;
using System.Collections;

public class Class_Warrior : Class_Char {

	// Use this for initialization
	public override void Start () {
        Attack = 5;
        Def = 4;
        Range_attack = 1;
        Range_move = 3;
        Attack_time = 1;
        Mp = 0;
        Name_class = "Warrior";
        Char_models.Add("weapon");
        Char_models.Add("shield");
        Char_models.Add("head");
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
