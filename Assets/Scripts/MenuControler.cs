﻿using UnityEngine;
using System.Collections;

public class MenuControler : MonoBehaviour {

    public bool exit = false;
	public LoadingHandler loadingHandler;

	public void OnButtonPlayClicked()
	{
		//loadingHandler.gameObject.SetActive(true);
	}
 
    public void Scenario()
    {
		//loadingHandler.gameObject.SetActive(true);
		Application.LoadLevel("Scenario1"); // LoadingScenes  level1
    }

	public void level_1()
	{
		// goi loading
		Application.LoadLevel ("level1");
	}

	public void level_2()
	{
		// goi loading
		Application.LoadLevel ("level2");
	}

	public void Shop()
	{
		// shop game
	}

    public void StartNewGame()
    {        
        Application.LoadLevel("MenuHome");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
