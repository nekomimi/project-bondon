﻿using UnityEngine;
using System.Collections;

public class Char_Enemy :Character {

	// Use this for initialization
	override public void Start () {
        base.Start();

        gameObject.AddComponent<Enemy_Move>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public override void Show_Action()
    {
        Manager_ui.Show_Menu_Action_Panel(null, false);
        //Tile_map.Find_Range_Move(transform.position, 3, Color.red);
        //base.Show_Action();
    }
    public override void Select_Char(bool open_infor)
    {
        base.Select_Char(open_infor);
        if (open_infor == true)
        {
            Tile_map.Find_Range_Move(transform.position, 3, Color.red);
        }
        else
            Tile_map.Find_Range_Move(transform.position, 3, Color.white);

    }
    public override void End_Turn()
    {
        
        base.End_Turn();
     //   Debug.Log("Enemy end turn");
        Mode_AI_Normal.Had_Target = false;
    }
}
