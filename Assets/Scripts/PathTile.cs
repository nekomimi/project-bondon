﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathTile : MonoBehaviour
{
    public int id;
    public string state = "";
	[HideInInspector] public List<PathTile> connections = new List<PathTile>();
   
}
