﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Class_Char: MonoBehaviour {

    protected string Name_class;
    protected int Hp=10;
    protected int Mp;
    protected int Attack;
    protected int Def;
    protected int Range_attack;
    protected int Range_move;
    protected int Attack_time;
    public List<string> Char_models;
    protected Character Char;
	// Use this for initialization
	virtual public void Start () {

        Char = GetComponent<Character>();
        Char.Name = Name_class;
        Char.Max_hp += Hp;
        Char.Cur_hp += Hp;
        Char.Max_mp += Mp;
        Char.Cur_mp += Mp;
        Char.Attack_point += Attack;
        Char.Def_point += Def;
        Char.Range_move += Range_move;
        Char.Rang_at += Range_attack;
        Char.Time_attack += Attack_time;
        foreach (string c in Char_models)
        {
            gameObject.transform.FindChild(Name_class + "_" + c).gameObject.GetComponent<SkinnedMeshRenderer>().enabled = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
