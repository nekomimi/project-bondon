﻿using UnityEngine;
using System.Collections;

public class Camera_Move : MonoBehaviour
{
    private Vector2 worldStartPoint;
    Transform Left_Top_archor;
    Transform Right_Bot_archor;

    public float speed = 0.1f;
    float horizontalSpeed=10;
    float verticalSpeed=10;
    private Vector3 velocity = Vector3.zero;
    void Start()
    {
        Left_Top_archor = GameObject.Find("Left_Top_Archor").transform;
        Right_Bot_archor = GameObject.Find("Right_Bottom_Archor").transform;
    }
    void Update()
    {

        // only work with one touch
       /* if (Input.touchCount == 1)
        {
            Touch currentTouch = Input.GetTouch(0);

            if (currentTouch.phase == TouchPhase.Began)
            {
                this.worldStartPoint = this.getWorldPoint(currentTouch.position);
                Debug.Log("Beagan");
            }

            if (currentTouch.phase == TouchPhase.Moved)
            {
                Debug.Log("Moved");
                Vector2 worldDelta = this.getWorldPoint(currentTouch.position) - this.worldStartPoint;

                Camera.main.transform.Translate(
                    -worldDelta.x,
                    -worldDelta.y,
                    0
                );
            }
        }*/
        
    }

    void LateUpdate()
    {
        /*if (Input.GetMouseButtonDown(1))
        {
            float h = horizontalSpeed * Input.GetAxis("Mouse Y") * Time.deltaTime;
            float v = verticalSpeed * Input.GetAxis("Mouse X") * Time.deltaTime;
            //Debug.Log(Input.GetAxis("Mouse Y"));
            if (transform.position.x + 2 >= Right_Bot_archor.position.x && Input.GetAxis("Mouse X") > 0)
            {
                v = 0;
            }
            if (transform.position.x + 2 <= Left_Top_archor.position.x && Input.GetAxis("Mouse X") < 0)
            {
                v = 0;
            }
            if (transform.position.z + 2 >= Left_Top_archor.position.z && Input.GetAxis("Mouse Y") < 0)
            {
                h = 0;
            }
            if (transform.position.z + 2 <= Right_Bot_archor.position.z && Input.GetAxis("Mouse Y") > 0)
            {
                h = 0;
            }


            // if((transform.position.x+2 <Right_Bot_archor.position.x && transform.position.x+2>Left_Top_archor.position.x)
            //   && (transform.position.z+2<Left_Top_archor.position.z && transform.position.z+2>Right_Bot_archor.position.z))
            transform.Translate(v, h, 0);
        }*/
        if (transform.position.x + 2 >= Left_Top_archor.transform.position.x)
        {
            transform.position += new Vector3(-speed, 0,0);
        }
        if (transform.position.x - 2 <= Right_Bot_archor.transform.position.x)
        {
            transform.position += new Vector3(speed, 0,0);
        }
        if (transform.position.z + 2 >= Left_Top_archor.transform.position.z)
        {
            transform.position += new Vector3(0, 0, -speed);
        }
        if (transform.position.z - 2 <= Right_Bot_archor.transform.position.z)
        {
            transform.position += new Vector3(0, 0, speed);
        }
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
           // transform.transform.position = Vector3.SmoothDamp(transform.position, new Vector3(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0),ref velocity, 0.3f);
            transform.Translate(-touchDeltaPosition.x * speed*Time.deltaTime, -touchDeltaPosition.y * speed*Time.deltaTime, 0);
        }

    }
    // convert screen point to world point
    private Vector2 getWorldPoint(Vector2 screenPoint)
    {
        RaycastHit hit;
        Physics.Raycast(Camera.main.ScreenPointToRay(screenPoint), out hit);
        return hit.point;
    }
}
