﻿using UnityEngine;
using System.Collections;

public class Class_Knight : Class_Char {

	// Use this for initialization
	public override void Start () {
        Attack = 6;
        Def = 4;
        Attack_time = 1;
        Mp = 0;
        Range_attack = 1;
        Range_move = 2;
        Name_class = "Knight";
        Char_models.Add("weapon");
        Char_models.Add("shield");
        Char_models.Add("head");
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
