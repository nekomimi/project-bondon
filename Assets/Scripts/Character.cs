﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour {

    public int Id;
    public int Index;
    public string Name;
    public int level;
    
    public int Max_hp;
    public int Cur_hp;
    public int Max_mp;
    public int Cur_mp;
    public int Attack_point;
    public int Def_point;
    public int Range_move;
    public int Rang_at;
    
    public Animator Anim;
    public Character Target;
    public bool Is_selected;
    //public bool On_turn;
    //public bool On_turn_action;
    public int Time_attack;
    public AudioSource Audio_source;
    public AudioClip Hit_sound;
    public AudioClip Dead_sound;
    public AudioClip Attack_sound;
    public string State_action;
    public string State_char;
    public string State_move;
    public Manager_UI Manager_ui;
    public Manager_Battle_Map Manager_battle;
    public TileMap Tile_map;
    public List<MyDictionary> Menu_function;
    public Vector3 Start_point;
    //public List<List<string>  Menu_function;
    public List<PathTile> path = new List<PathTile>();
    
    int Time_current_attack;
	// Use this for initialization
	virtual public void Start () {
        
        Manager_battle = GameObject.Find("Manager_Battle").GetComponent<Manager_Battle_Map>();
        Manager_ui = GameObject.Find("Manager_UI").GetComponent<Manager_UI>();
        Tile_map = GameObject.Find("Tilemap").GetComponent<TileMap>();
        Start_point = transform.position;
        this.Tile_map.Update_Position(Start_point, Start_point);
       // Debug.Log("Update position");
        Anim = GetComponent<Animator>();
        State_char = "Used";
	}
	
	// Update is called once per frame
	void Update () {
	
       // if(State_action=="end_turn" && Manager_Battle_Map.Turn_Forces=="Player");
        //if (State_action != "Move") return;
        Handle_Input();
	}
    void Handle_Input()
    {
      
    }
    public void On_Damage_Attack()
    {
        
        Target.On_Be_Attack(Attack_point);
    }
    public void On_Be_Attack(int Damage)
    {
        Cur_hp -= Damage;
        if (Cur_hp <= 0)
        {
            //Debug.Log(transform.parent.name + " is Dedd");
            this.Anim.SetInteger("state", 3);
            State_char = "Dead";
            Tile_map.Update_Position_Dead(transform.position);
           // Manager_battle.End_Turn(Index,"Dead");
            return;
        }

        this.Anim.SetInteger("state",2);
    }
    public void On_Complete_Hit_State()
    {
        this.Anim.SetInteger("state", 0);

    }

    public void Start_Attack_Target()
    {
      //  Debug.Log("Start attack");
        /*if (Target.State_char == "Dead")
        {
            Debug.Log("Target is Dead");
            Manager_battle.End_Turn(Index, "Used", Start_point, Start_point);
            return;
            Debug.Log("Target is Dead");
            End_Turn();
            return;
        }*/
       
            transform.LookAt(Target.transform.position);
            Target.To_Be_Target(this);
            this.Anim.SetInteger("state", 4);
          //  Debug.Log("Attack target "+Time_attack);
    
           /* if (Manager_Battle_Map.Total_turn == 0)
            {
                End_Turn();
              //  Manager_battle.Change_Turn_Action_For_Target(Target);
                //Manager_battle.End_Action();
             //   Debug.Log("End turn");
               // Manager_battle.End_Turn(Index, "Used",Start_point,Start_point);
            }
            else
            {*/

           
           // }
        
      //  Debug.Log("Testing state target : " + Target.State_char);
    }
    public void To_Be_Target(Character enemy )
    {
        transform.LookAt(enemy.transform.position);
        Target = enemy;
    }
    public void On_Complete_Attack_State()
    {
        this.Anim.SetInteger("state", 0);
         this.Time_current_attack--;
         if (Target.State_char == "Dead"){
             Tile_map.Update_Position_Dead(Target.transform.position);
             End_Turn();
         }
        else{
             if(Target.Time_current_attack>0)
                 StartCoroutine(Change_Turn());
             else
             {
                 Target.End_Turn();
                 End_Turn();
                
             }

         }
    }
    public void On_Complete_Dead_State()
    {
        //State_char = "Dead";
       // Debug.Log(transform.parent.name + "Dead");
       // State_char = "Dead";
        gameObject.SetActive(false);
        //Manager_battle.End_Turn(Start_point, Start_point);
        //Debug.Log("Dead");
    }
    IEnumerator Change_Turn()
    {
     
        yield return new WaitForSeconds(1);
       
        Manager_battle.Change_Turn_Action_For_Target(Target);
        
    }
    public void On_Turn_Action()
    {
       /* if (State_char == "Dead")
        {
            Target.End_Turn();
            //Manager_battle.End_Action();
            Tile_map.Update_Position_Dead(transform.position);
            //Manager_battle.End_Turn(Start_point, Start_point);
            return;
        }*/
        
        if (this.Target != null )
        {
            //Debug.Log(transform.parent.name + " re attack ");
            Start_Attack_Target();
        }
            
    }
    virtual public void Select_Char(bool open_infor)
    {
        if (open_infor == true)
        {
            Manager_ui.Show_Infor_Panel(this, true);
            Show_Action();    


        }
        else { 
            Manager_ui.Show_Infor_Panel(this, false);
            Manager_ui.Show_Menu_Action_Panel(null, false);
            
        }
    }
    virtual public void Show_Action()
    {

    }
   
    public void Start_Combat(Character target)
    {
      //  Debug.Log("Start combat");
       // Manager_Battle_Map.Total_turn = this.Time_attack+target.Time_attack;
       // Debug.Log("Time attack " + Manager_Battle_Map.Total_turn);
        this.Time_current_attack = this.Time_attack;
        target.Time_current_attack = target.Time_attack;
        Start_Attack_Target();
        //Manager_Battle_Map.Turn_of_2st = target.Time_attack;
    }
    virtual public void End_Turn()
    {
       // Debug.Log(Name + " is End Turn ");
        Select_Char(false);
        //Debug.Log(transform.position);
        this.State_char = "Used";
        Target = null;
        Tile_map.Update_Position(Start_point, transform.position);
        Manager_battle.End_Action();
    }

  
}
