﻿using UnityEngine;
using System.Collections;

public class Class_Fighter : Class_Char {

	// Use this for initialization
	public override void Start () {
        Mp = 0;
        Attack = 5;
        Def = 3;
        Range_attack = 1;
        Range_move = 2;
        Attack_time = 1;
        Name_class = "Fighter";
        Char_models.Add("weapon");
        Char_models.Add("head");
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
