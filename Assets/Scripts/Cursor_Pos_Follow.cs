﻿using UnityEngine;
using System.Collections;

public class Cursor_Pos_Follow : MonoBehaviour {

	// Use this for initialization
    public Transform Pos_Target;
    public RectTransform Canvas;
    RectTransform rectTransform;
	void Start () {
        rectTransform = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Canvas == null)
            return;
        float delx = Canvas.sizeDelta.x / Screen.width;
        float dely = Canvas.sizeDelta.y / Screen.height;
        if (Pos_Target == null)
            return;
        Vector3 screenPos = Camera.main.WorldToScreenPoint(Pos_Target.position);
        rectTransform.anchoredPosition = new Vector2(screenPos.x * delx, screenPos.y * dely);
        //Debug.Log("Testing " + screenPos.x * delx+" "+ screenPos.y * dely);
	
	}
}
