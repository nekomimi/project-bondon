﻿using UnityEngine;
using System.Collections;

public class Class_Mage : Class_Char {

	// Use this for initialization
	void Start () {
        Attack = 4;
        Def = 1;
        Attack_time = 1;
        Range_attack = 3;
        Range_move = 2;
        Mp = 10;
        Name_class = "Mage";
        Char_models.Add("staff");
        Char_models.Add("head");
        Char_models.Add("book");
        base.Start();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
