﻿using UnityEngine;
using System.Collections;


public class Char_Player : Character {

	// Use this for initialization
    public int Max_ex;
    public int Cur_ex;
    //string Action_selected;
   
 //   ublic Vector3 Start_point;
	override public void Start () {
        gameObject.AddComponent<Player>();
        base.Start();
        GetComponent<Player>().character = this;
        
        Menu_function.Add(new MyDictionary("Attack", false));
        Menu_function.Add(new MyDictionary("Move", false));
        Menu_function.Add(new MyDictionary("Skill", false));
        Menu_function.Add(new MyDictionary("End turn", false));
        Menu_function.Add(new MyDictionary("Back", false));
     /*   Menu_function.Add("Attack", false);
        Menu_function.Add("Move", false);
        Menu_function.Add("Skill", false);
        Menu_function.Add("End turn", false);
        Menu_function.Add("Back",true);
        Debug.Log(Menu_function["Attack"]);*/
       // Tile_map.Find_Range_Move(transform.position, 2);
	    
	}
	
	// Update is called once per frame
	void Update () {

        Input_Handle();
	}
    void Input_Handle()
    {

        if (State_action == "Attack" &&Anim.GetInteger("state")!=4)
        {
            bool miss = false;
            if(Input.GetMouseButtonDown(0))
            {
                 var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                 RaycastHit hitInfo;
               // Debug.Log("Testing pos "+ r)
                 if (Physics.Raycast(ray, out hitInfo, 1000, LayerMask.GetMask("Enemy_force")))
                 {
                     Debug.Log(hitInfo.transform.name);
                     Tile_map.FindPath(transform.position, hitInfo.transform.position, path);
                     Show_Range_Attack(Color.white);
                     //if (path.Count > Rang_at+1 || path.Count==0)
                     // return;
                     Target = hitInfo.transform.gameObject.GetComponent<Character>();
                     Start_Combat(Target);
                     State_action = "Attacked";
                     //Start_Attack_Target();

                 }
             
                 else
                     miss = true;
            }
            if (miss == true)
            {
                Show_Range_Attack(Color.white);
                Back_To_Start_Point();
            }
        }
    }
    public override void Show_Action()
    {
        if (State_char == "Unused")
        {
            Manager_ui.Show_Menu_Action_Panel(Menu_function, true);
        }
    }
    public  void Select_Action(string action)
    {
        //bool show=false;
        if (action == "")
        {
           // show =true;
            return;
        }
        else
        {
            
            State_action=action;
            
            switch(action)
            {
                case "Move":
                    
                    Show_Range_Move(Color.blue);
                    //Debug.Log("action "+  Menu_function[Search_Index_Action("Move")].Name);
                   // Debug.Log(Menu_function.IndexOf(new MyDictionary("Move", false)));
                    Menu_function[Search_Index_Action("Move")].Is_active = true;
                  //  Debug.Log(Menu_function.IndexOf(new MyDictionary("Move", false)));
                    Select_Char(false);
                    break;
                case "End turn":
                    End_Turn();
                   
                    break;
                case "Attack":
                    Select_Char(false);
                    Show_Range_Attack(Color.yellow);
                    break;
                case "Back":
                    Back_To_Start_Point();
                    break;
               
            }
            Menu_function[Search_Index_Action("Back")].Is_active = false;

           
        }
        //Debug.Log(" show " + show);

        //Manager_ui.Show_Menu_Action_Panel(Menu_function,show);
      
       // Manager_ui.Show_Infor_Panel(this,show);
        //else if(action=="close_menu_action")
          //  Manager_ui.Show_Menu_Action_Panel(State_action,false);

    }
    int Search_Index_Action(string key)
    {
        int index=-1;
        for (int i = 0; i < Menu_function.Count; i++)
        {
            if (Menu_function[i].Name == key)
            {
                index = i;
                break;
            }
        }
        return index;
    }
    public void Back_To_Start_Point()
    {
        gameObject.transform.position = Start_point;
       
        //Menu_function[Search_Index_Action("Back")].Is_active = false;
        Menu_function[Search_Index_Action("Move")].Is_active = false;
        State_move = "";
        State_action = "";
        Select_Char(false);
        Manager_battle.Player_force.Char_selected = null;
    }
    override public void End_Turn()
    {
        base.End_Turn();
       
        //Manager_battle.Player_force.Char_selected = null;
        //Manager_battle.End_Turn(Start_point, transform.position);
       
    }
    public void Show_Range_Move(Color color)
    {
       Tile_map. Find_Range_Move(transform.position,Range_move,color);
    }
    public void Show_Range_Attack(Color color)
    {
         Tile_map.Find_Range_Attack( transform.position,Rang_at,color);
    }


 
}
