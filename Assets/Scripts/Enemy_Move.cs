﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy_Move : MonoBehaviour {
    public float walkSpeed;
    public Character character;
    TileMap tileMap;
    List<PathTile> path = new List<PathTile>();
    
   // LineRenderer lineRenderer;
  //  public string state;

    void Start()
    {
        walkSpeed = 3;

     //   lineRenderer = GetComponent<LineRenderer>();
        tileMap = FindObjectOfType(typeof(TileMap)) as TileMap;
        enabled = tileMap != null;
        character = GetComponent<Character>();


    }
    public void Move_To_Target(Char_Player Target)
    {
        if(Target!=null)
            character.Target = Target;
      //  Debug.Log("Testing :" + character.Name + " + " + Target.Name);
        Vector3 endpos = tileMap.Find_Path_In_Range(transform.position, Target.transform.position);
        StartCoroutine(WaitEndpos(endpos));
    }
    public void Move_To_Pos(Vector3 Endto)
    {
        //Vector3 endpos = tileMap.Find_Path_In_Range(transform.position,Endto);
        StartCoroutine(WaitEndpos(Endto));
    }
    void Update()
    {
        
    }
    IEnumerator WaitEndpos(Vector3 pos)
    {

        yield return new WaitForSeconds(1); 
        tileMap.FindPath(transform.position,pos, path);
       // Debug.Log("Testing path :" + path.Count);
        character.Anim.SetInteger("state", 1);
        StopAllCoroutines();
        StartCoroutine(WalkPath());
    }
    IEnumerator WalkPath()
    {
       // Debug.Log("Move ing");
        var index = 0;
        while (index < path.Count)
        {
            yield return StartCoroutine(WalkTo(path[index].transform.position));
            index++;
        }
        character.State_move = "End move";
        character.Anim.SetInteger("state", 0);
        StartCoroutine(WaitIdle());
        //On_End_Move();
        //character.Select_Char(true);
      
    }
    public void On_End_Move()
    {
        if (Find_Target_In_Range_Attack())
            character.Start_Combat(character.Target);
        else
        {
           
            character.End_Turn();
        }
    }
    IEnumerator WaitIdle()
    {
        yield return new WaitForSeconds(3);
        if (character.Target == null)
            character.End_Turn();
        else
        On_End_Move();
    }
    IEnumerator WalkTo(Vector3 position)
    {
        while (Vector3.Distance(transform.position, position) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, position, walkSpeed * Time.deltaTime);
            transform.LookAt(position);
            yield return 0;
        }
        transform.position = position;
        
    }
    bool Find_Target_In_Range_Attack()
    {
        bool result=false;
        tileMap.FindPath(transform.position, character.Target.transform.position,path);
        if (path.Count <= character.Rang_at + 1)
        {
            result = true;
        }

        return result;
    }
    
}
