﻿using UnityEngine;
using System.Collections;

public class Class_Warlock : Class_Char {

	// Use this for initialization
	void Start () {
        Attack = 3;
        Def = 3;
        Attack_time=1;
        Range_attack = 2;
        Range_move = 2;
        Mp = 10;

        Name_class = "Warlock";
        Char_models.Add("staff");
        Char_models.Add("head");
        base.Start();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
