﻿using UnityEngine;
using System.Collections;

public class Manager_Enemy : Manager_Forces{

	// Use this for initialization
	public override void Start () {
        Name_force = "Enemy";
        base.Start();
    
      
	}
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
    //    Debug.Log("Testing " + State_forces);
       
	}
    public override void Handle()
    {
        base.Handle();
   

    }
    public override void Start_Active()
    {
        base.Start_Active();

    }
    public void Move_Selected_Char(Char_Player Target)
    {
        Char_selected.GetComponent<Enemy_Move>().Move_To_Target(Target);
       
    }
    public void Move_Selected_Char(Vector3 Endto)
    {
        Char_selected.GetComponent<Enemy_Move>().Move_To_Pos(Endto);
    }

    public void Attack_Selected_Char(Char_Player Target)
    {
        Char_selected.Target = Target;
        Char_selected.Start_Combat(Target);
        //Char_selected.On_Turn_Action();
       // Char_selected.Start_Attack_Target();
     //   Debug.Log("Testing name target " +Char_selected.Target.Name);
    }
    
}
